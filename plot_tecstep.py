import matplotlib.pyplot as plt

# Open the file in read mode
with open("tecpid.txt", "r") as f:
    # Read the contents of the file
    data = f.read()

# Split the data into rows
rows = data.split("\n")

if rows[-1] == "":
    rows.pop()

# Initialize lists to store the x and y data for each set
x_data = []
y_data = [[] for _ in range(8)]

# Iterate over each row
for i, row in enumerate(rows):
    # Split the row by the comma
    values = row.split(",")
    
    # Check that there are at least 6 values
    if len(values) < 6:
        print(f"Error: row {i+1} has less than 6 values")
        continue
    
    # Append the first value (time point) to the x_data list
    try:
        x_data.append(float(values[0].strip()))
    except ValueError:
        print(f"Error: could not convert value to float in row {i+1}")
        continue
    
    # Append the sixth value to the y_data list for the corresponding set
    for j in range(8):
        try:
            y_data[j].append(float(values[(6*(j+1))-1].strip())) # change here which data you want(2 or 6 (with UI or not))
        except ValueError:
            print(f"Error: could not convert value to float in row {i+1}")

# Iterate over each data set
for y in y_data:
    # Plot the x and y data
    plt.plot(x_data, y)

# Show the plot
plt.show()
